class UsersController < ApplicationController
  before_action :authenticate_user!
  before_action :correct_user,  only: [:show, :update, :edit]
  before_action :admin_user,    only: [:shuffle]

  def index
    @users = User.all
  end

  def show
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash.now[:info] = 'Wishlist updated.'
      render 'show'
    else
      flash[:danger] = 'Failed to update wishlist.'
      redirect_to 'show'
    end
  end

  def shuffle
    begin
      @user = current_user
      User.update_all recipient_id: nil
      User.update_all assigned: false
      users = User.all
      users.each do |user|
        recipients = users.where(["id != ? and NOT assigned", user])
        recipient = recipients.sample
        recipient.update_attribute(:assigned, true)
        user.recipient = recipient
        user.save unless recipient.nil?
      end
      flash[:success] = 'Santas assigned.'
    rescue NoMethodError => e
      flash[:danger] = 'Somebody got assigned to himself. Shuffle again.'
    end
    redirect_to user_path
  end

  private

    def user_params
      params.require(:user).permit(:name, :wishlist, :photo)
    end

    def is_logged_in?
      unless user_signed_in?
        flash[:info] = 'Please sign in first.'
        redirect_to new_user_session_path
      end
    end

    def correct_user
      @user = User.find(params[:id])
      redirect_to root_url unless @user == current_user
    end

    def admin_user
      redirect_to root_url unless current_user.admin?
    end
end