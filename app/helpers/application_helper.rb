module ApplicationHelper

  # Returns full title
  def full_title(page_title = '')
    base = 'Secret Santa App'
    if page_title.empty?
      base
    else
      page_title + ' | ' + base
    end
  end
end
