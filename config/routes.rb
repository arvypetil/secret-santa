Rails.application.routes.draw do
  devise_for :users
  resources  :users, only: [:index, :show, :update]
  post 'users/:id', to: 'users#shuffle'
  get '/about', to: 'pages#about'
  root to: 'pages#home'
end
