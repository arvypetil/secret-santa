class AddWishlistTextToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :wishlist, :string
  end
end
