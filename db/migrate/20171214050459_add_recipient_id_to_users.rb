class AddRecipientIdToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :recipient_id, :integer
  end
end
