class AddAssignedColumnToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :assigned, :boolean, default: false
  end
end
