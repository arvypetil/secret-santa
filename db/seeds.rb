# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create!(name: "Admin",
             email: "user1@example.com",
             wishlist: "Shoes",
             password: "foobar",
             password_confirmation: "foobar",
             admin: true)
             
User.create!(name: "Baron",
             email: "user2@example.com",
             wishlist: "Shirts",
             password: "foobar",
             password_confirmation: "foobar")
            
User.create!(name: "Henry",
             email: "user3@example.com",
             wishlist: "Glasses",
             password: "foobar",
             password_confirmation: "foobar")

User.create!(name: "John",
             email: "user4@example.com",
             wishlist: "Pants",
             password: "foobar",
             password_confirmation: "foobar")